/**
 * Lavender magic
 */

/**
 * Lang manager class
 */
class Lang {
    constructor(lang) {
        this._pathes = [];
        this._iter = 0;

        this.serialize = {};

        this.keyGen(lang);
    }

    /**
     * Get localization text by key
     * 
     * @param {String} key 
     */
    _get(key) {
        if (this.serialize.hasOwnProperty(key))
            return this.serialize[key];

        console.warn(`[v-lang] Not found key '${key}' in langs!`);
        return '';
    }

    /**
     * Generate full inline keys for object values
     * 
     * @param {Object} object 
     * @param {Boolean} inside  recursive
     * @param {String} path     object parent key for next child 
     */
    keyGen(object, inside = false, path = false) {
        // First iterator object key
        // Clear meta data
        if (!inside) {
            this._keys = [];
            this._depth = 0;
            this.serialize = {};
        }

        for (let key in object) {
            // Check key is root
            if (this._keys[this._depth] == void 0)
                this._keys[this._depth] = path || "";

            if (typeof object[key] === "object") {
                this._keys[this._depth] += `${key}.`;
                this.keyGen(object[key], true, this._depth != 0 ? this._keys[this._depth] : "");

            } else {
                // If value is not object

                this._keys[this._depth] += key;
                this.serialize[this._keys[this._depth]] = object[key];
                this._depth++;
            }
        }
    }
}


export default Lang;