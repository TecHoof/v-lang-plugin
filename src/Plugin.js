/**
 * Lavender magic
 */

import Manager from './Manager';


/**
 * General class. Injected in Vue instance
 */
class Plugin {
    /**
     * Required function for vue plugin 
     * 
     * @param {Vue} Vue 
     * @param {Object} langs 
     * @param {String} defaultLang 
     */
    install(Vue, langs, defaultLang) {
        // Create single instance manager
        let manager = new Manager(langs, defaultLang);

        Vue.mixin({
            beforeCreate: function () {
                Vue.util.defineReactive(this, '$vlang', manager);
            }
        });
    }
}


export default new Plugin();