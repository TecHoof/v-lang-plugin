/**
 * Lavender magic
 */

class Manager {
    constructor(langs, defaultLang) {
        this._langs = langs;
        this.list = Object.keys(this._langs);

        this.current = defaultLang || this._langs[0];
    }

    /**
     * Switch lang
     * 
     * @param {*} lang 
     */
    set(lang) {
        this.current = lang;
    }

    /**
     * Get localization text by key
     * 
     * @param {String} key 
     */
    get(key) {
        return this._langs[this.current]._get(key);
    }
};


export default Manager;