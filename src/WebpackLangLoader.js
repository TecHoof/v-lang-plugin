/**
 * Lavender magic
 */

import Lang from './Lang';


/**
 * Loader languages for webpack
 */
function WebpackLangLoader() {
    // Get all files from langs folder
    // See in doc, how configure path:
    // https://www.npmjs.com/package/v-lang-plugin#configure-lang-folder-for-webpack
    let lContext = require.context('langs', false, /\.js$/);

    let langs = {};

    lContext.keys().forEach((key) => {
        let lang = lContext(key); // require lang file
        langs[lang.default.langName] = new Lang(lang.default);
    });

    return langs;
}


export default WebpackLangLoader;