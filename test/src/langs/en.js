export default {
  langName: "en",

  projectName: "v-lang-plugin",

  pages: {
    home: {
      projectLinkDescription: "For a guide and recipes on how to configure / customize this project, check out the",
      documentation: "documentatio1n"
    }
  }
};
