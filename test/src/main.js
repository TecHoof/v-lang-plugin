import Vue from 'vue';
import App from './App.vue';

import { VLang, WebpackLangLoader } from 'v-lang-plugin';

Vue.config.productionTip = false;
Vue.use(VLang, WebpackLangLoader(), 'en');

new Vue({
  render: h => h(App),
}).$mount('#app');
