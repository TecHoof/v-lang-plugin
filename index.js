/**
 * Lavender magic
 */

import WebpackLangLoader from './src/WebpackLangLoader';
import VLang from './src/Plugin';


export {
    VLang,
    WebpackLangLoader
};