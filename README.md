### Installation
```bash
$ npm install v-lang-plugin --save
```


### Get started
```js
import Vue from 'Vue';
import { VLang, WebpackLangLoader } from 'v-lang-plugin';

Vue.use(VLang, WebpackLangLoader(), 'en');
```
Vue.use(VLang, LangLoader, lang | default - first lang in list);


### Configure lang folder for webpack
Please, create alias in your webpack config file:
```js
module.exports = {
    resolve: {
        alias: {
            'langs': './path-to-your-langs-folder'
        }
    }
}
```


### How work without loaders?
```js
const locales = {
    'en': {
        'langName': 'en',
        'magic': 'Pony'
    },
    'ru': {
        'langName': 'ru',
        'magic': 'Пони'
    },
    ...
};

Vue.use(VLang, locales);
```


### Structure langs folder and file
```
langs
├── en.js
├── ru.js
└── ...
```

```js
module.exports = {
    'langName': 'en', // Requirement param
    'pages': {
        'home': {
            'title': 'Main site'
        }
    },
    'magic': 'Pony'
};
```


### How use langs?
```js
$vlang.get('pages.home.title'); // Return 'Main site'
$vlang.get('magic'); // Return 'Pony'
$vlang.get(); // Return '' and write warn to console 

$vlang.set('en'); // Set language for reactive update all labels

$vlang.list // Array of available langs. Example: ['en', 'ru', ...]
$vlang.current // Current lang. Return: 'en'
```